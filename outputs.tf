output "jumphost_public_ip" {
  description = "public ip of jumphost"
  value       = module.f5-rs-aws-tgw-network.jumphost_public_ip
}

output "dev_docker_a_public_ip" {
  description = "public ip of dev_docker_a"
  value       = module.dev-docker-a.docker_host_public_ip
}

output "dev_docker_b_public_ip" {
  description = "public ip of dev_docker_b"
  value       = module.dev-docker-b.docker_host_public_ip
}

output "bigip_mgmt_internet_ip" {
  description = "mgmt_public_ips of the BIGIP"
  value       = module.bigip.mgmt_internet_ip
}

output "bigip_private_ip" {
  description = "bigip_private_ip of the BIGIP"
  value       = module.bigip.private_addresses[0]
}

output "bigip_public_ip" {
  description = "bigip_public_ip of the BIGIP"
  value       = module.bigip.public_addresses[0]
}

output "public_addresses" {
  description = "public_addresses of the BIGIP"
  value       = module.bigip.public_addresses
}