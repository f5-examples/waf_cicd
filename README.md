Deploying this example:
1. create a seperate PRIVATE repo with the name iac_state in your gitlab account. that will be used to store terraform state (for the automated deployments)
2. make sure the repo you created is PRIVATE as it will store sensitive state information
3. fork this repo 
4. go to the project settings in gitlab and configure the following environment variables: (those are private variables which only the repo owner has access to)
TF_VAR_AWS_ACCESS_KEY_ID - your AWS access key 
TF_VAR_AWS_SECRET_ACCESS_KEY - your AWS secret key 
TF_VAR_AWS_DEFAULT_REGION - AWS default region 
GITLAB_USER_EMAIL - your personal gitlab email address. this is used when pushing terraform state into your private repo
IAC_ACTION - the value will determine if deploying or destroying. values are deploy / destroy 
IAC_STATE_REPO_WITH_PASS - used to store state - your private repo in the format of https://git_username:git_password@gitlab.com/your_username/iac_state.git
TF_VAR_BIGIP_PWD - password for the bigip admin account 
TF_VAR_PUBLIC_KEY - your public ssh key for creating a key-pair in AWS.
TF_VAR_USERNAME - a username used to tag your resources 
5. login to your AWS account, subscribe  to the bigip image used in the demo - https://aws.amazon.com/marketplace/pp/B07F273HD7/ 
6. run the pipeline 

this example deploys a shared services vpc which will host your bigip, dev vpc which hosts 2 docker hosts running 'juiceshop' app, a preprod vpc and a 'prod' vpc. 
vpc's are connected with transit gateway. 

